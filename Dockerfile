FROM node:latest
MAINTAINER '45incode@gmail.com'
RUN git clone https://gitlab.com/xlvcode/draft-server.git
COPY path/package*.json /app/
WORKDIR /app
RUN npm i --production
COPY . .
EXPOSE 3000
CMD ["npm","start"]